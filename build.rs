use ptx_builder::error::Result;
use ptx_builder::prelude::*;
use std::env::{var, set_var};

fn main() -> Result<()> {
    if var("CARGO_FEATURE_CUDA").is_ok() {
        // Workaround for "crate required to be available in rlib format" bug
        set_var("CARGO_BUILD_PIPELINING", "false");

        // Help cargo find libcuda
        println!("cargo:rustc-link-search=native=/opt/cuda/lib64/");

        // Build ptx kernel
        let builder = Builder::new("core")?;
        CargoAdapter::with_env_var("KERNEL_PTX_PATH").build(builder);
    } else {
        Ok(())
    }
}
