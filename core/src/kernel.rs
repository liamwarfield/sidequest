use super::{DeviceBufferRef, Engine, CameraRay, ViewParams, KernelParams};
use nalgebra as na;

#[no_mangle]
pub extern "ptx-kernel" fn render(
    params_ptr: *mut KernelParams,
) {
    use core::arch::nvptx::*;

    let params = unsafe { &*params_ptr };
    let x = unsafe { _block_dim_x() * _block_idx_x() + _thread_idx_x() } as usize;
    let y = unsafe { _block_dim_y() * _block_idx_y() + _thread_idx_y() } as usize;
    let w = params.view.width as usize;
    let h = params.view.height as usize;
    if x >= w || y >= h { return }

    let engine = params.engine.map_buffer(&mut |b| unsafe { b.to_slice() });
    let mut rng = super::rng_at(params.seed as usize, x, y);
    let color = engine.render_ray(params.view.camera_ray(x, y), &mut rng);

    let ptr = unsafe { (*params_ptr).buffer.as_raw_mut() };
    let i = x + y * w;
    unsafe {
        core::slice::from_raw_parts_mut(ptr.offset((i * 3) as isize), 3)
    }.copy_from_slice(color.as_ref());
}

#[panic_handler]
fn panic(info: &::core::panic::PanicInfo) -> ! {
    use core::arch::nvptx::*;

    unsafe { trap() }
}
