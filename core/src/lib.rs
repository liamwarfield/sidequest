#![no_std]
#![cfg_attr(
    any(target_arch = "nvptx", target_arch = "nvptx64"),
    feature(abi_ptx, stdsimd)
)]

use core::f32::consts::PI;
use core::hash::{Hash, Hasher};
use core::ops::{Deref, DerefMut};
use na::{Isometry3, Matrix3, Point3};
use nalgebra as na;
use num_traits::float::Float;
use rand::distributions::Open01;
use rand::Rng;
use rustacuda_core::{DeviceCopy, DevicePointer};
use rustacuda_derive::DeviceCopy;

#[cfg(any(target_arch = "nvptx", target_arch = "nvptx64"))]
mod kernel;

const HALF_PI: f32 = PI / 2.;
type IVec2 = na::Vector2<usize>;
type Vec2 = na::Vector2<f32>;
type Vec3 = na::Vector3<f32>;

#[inline(always)]
fn vec2<N: na::Scalar>(x: N, y: N) -> na::Vector2<N> {
    na::Vector2::new(x, y)
}

#[inline(always)]
fn vec3<N: na::Scalar>(x: N, y: N, z: N) -> na::Vector3<N> {
    na::Vector3::new(x, y, z)
}

pub mod aces {
    // This fit for the ACES curve was created by Stephen Hill (@self_shadow).

    use super::Vec3;
    use nalgebra::Matrix3;

    #[rustfmt::skip]
    pub fn input_mat() -> Matrix3<f32> {
        Matrix3::new(
            0.59719, 0.35458, 0.04823,
            0.07600, 0.90834, 0.01566,
            0.02840, 0.13383, 0.83777,
        )
    }

    #[rustfmt::skip]
    pub fn output_mat() -> Matrix3<f32> {
        Matrix3::new(
            1.60475, -0.53108, -0.07367,
            -0.10208, 1.10813, -0.00605,
            -0.00327, -0.07276, 1.07602,
        )
    }

    pub fn rtt2odt(v: f32) -> f32 {
        let a = v * (v + 0.0245786) - 0.000090537;
        let b = v * (0.983729 * v + 0.4329510) + 0.238081;
        a / b
    }

    pub fn pixel_to_ldr(value: Vec3) -> Vec3 {
        output_mat() * (input_mat() * value).map(rtt2odt)
    }

    pub fn to_ldr(high_buffer: &[f32], low_buffer: &mut [u8], exposure: f32) {
        assert_eq!(high_buffer.len() * 4, low_buffer.len() * 3);
        for (h, l) in high_buffer.chunks(3).zip(low_buffer.chunks_mut(4)) {
            let high = Vec3::from_column_slice(h);
            let low = pixel_to_ldr(high * exposure);
            let low = low.map(|x| (x.min(1.).max(0.) * 255.) as u8);
            l.copy_from_slice(&[low.z, low.y, low.x, 255]); // BGRA
        }
    }

    #[test]
    #[rustfmt::skip]
    fn nalgebra_new_row_major() {
        use super::vec3;

        let m = Matrix3::new(
            0., 0., 1.,
            0., 0., 0.,
            0., 0., 0.,
        );
        assert_eq!(m * vec3(0., 0., 1.,), vec3(1., 0., 0.));
    }
}

pub trait HasDeviceCopy {
    type Repr: Copy + DeviceCopy;

    fn from_copy(repr: Self::Repr) -> Self;
    fn to_copy(self) -> Self::Repr;
}

impl HasDeviceCopy for Vec3 {
    type Repr = [f32; 3];

    #[inline(always)]
    fn from_copy(repr: [f32; 3]) -> Self {
        repr.into()
    }

    #[inline(always)]
    fn to_copy(self) -> [f32; 3] {
        self.into()
    }
}

impl HasDeviceCopy for Point3<f32> {
    type Repr = [f32; 3];

    #[inline(always)]
    fn from_copy(repr: [f32; 3]) -> Self {
        repr.into()
    }

    #[inline(always)]
    fn to_copy(self) -> [f32; 3] {
        self.coords.into()
    }
}

impl HasDeviceCopy for Isometry3<f32> {
    type Repr = [f32; 7];

    #[inline(always)]
    fn from_copy(repr: [f32; 7]) -> Self {
        let t = Vec3::from_column_slice(&repr[..3]);
        let r = na::Vector4::from_column_slice(&repr[3..]);
        Isometry3 {
            translation: t.into(),
            rotation: na::Unit::new_unchecked(r.into()),
        }
    }

    #[inline(always)]
    fn to_copy(self) -> [f32; 7] {
        let mut repr = [0.; 7];
        repr[..3].copy_from_slice(self.translation.vector.as_ref());
        repr[3..].copy_from_slice(self.rotation.as_vector().as_ref());
        repr
    }
}

#[derive(Copy, Clone, Debug)]
#[repr(transparent)]
pub struct Cpy<T: HasDeviceCopy> {
    pub inner: T::Repr,
}

// Cpy is transparent T::Repr -> guaranteed by HasDeviceCopy to be DeviceCopy
unsafe impl<T: HasDeviceCopy> DeviceCopy for Cpy<T> {}

impl<T: HasDeviceCopy> Cpy<T> {
    #[inline(always)]
    pub fn to(&self) -> T {
        T::from_copy(self.inner)
    }
}

impl<T: HasDeviceCopy> From<T> for Cpy<T> {
    #[inline(always)]
    fn from(val: T) -> Cpy<T> {
        Cpy {
            inner: T::to_copy(val),
        }
    }
}

#[derive(Copy, Clone, DeviceCopy)]
#[repr(C)]
pub struct DeviceBufferRef<T> {
    pub data: DevicePointer<T>,
    pub len: u32,
}

impl<T> DeviceBufferRef<T> {
    pub fn from_raw(data: DevicePointer<T>, len: u32) -> Self {
        DeviceBufferRef { data, len }
    }

    pub unsafe fn to_slice<'a>(self) -> &'a [T] {
        core::slice::from_raw_parts(self.data.as_raw(), self.len as usize)
    }

    pub unsafe fn to_mut_slice<'a>(mut self) -> &'a mut [T] {
        core::slice::from_raw_parts_mut(self.data.as_raw_mut(), self.len as usize)
    }
}

#[derive(DeviceCopy, Clone)]
#[repr(C)]
pub struct KernelParams {
    pub seed: u32,
    pub view: ViewParams,
    pub engine: Engine<DeviceBufferRef<f32>>,
    pub buffer: DevicePointer<f32>,
    pub len: u32,
}

#[derive(Clone, Debug, Copy)]
pub struct Ray {
    pub origin: Point3<f32>,
    pub dir: Vec3,
}

pub type Spectrum = Vec3;

impl Ray {
    pub fn at(self, t: f32) -> Point3<f32> {
        self.origin + self.dir * t
    }
}

#[derive(Copy, Clone, Debug)]
pub struct SkyVertex {
    pub direction: Vec3,
    pub energy: Spectrum,
}

#[derive(Copy, Clone, Debug)]
pub struct SurfaceVertex {
    pub point: Point3<f32>,
    pub normal: Vec3,
    pub tangent: Vec3,
    pub albedo: Spectrum,
    pub fresnel0: f32,
    pub roughness: f32,
    pub depth: f32,
}

pub type CameraVertex = Ray;

#[derive(Clone, Copy, Debug, DeviceCopy)]
#[repr(C)]
pub struct Perspective {
    pub left: f32,
    pub right: f32,
    pub top: f32,
    pub bottom: f32,
    pub near: f32,
    pub far: f32,
}

#[derive(Clone, Debug, DeviceCopy)]
#[repr(C)]
pub struct ViewParams {
    pub perspective: Perspective,
    pub isometry: Cpy<Isometry3<f32>>,
    pub width: u32,
    pub height: u32,
}

impl ViewParams {
    #[rustfmt::skip]
    pub fn camera_ray(&self, x: usize, y: usize) -> CameraRay {
        let p = self.perspective;
        let i = self.isometry.to();
        let cx = x as f32 / self.width as f32;
        let cy = y as f32 / self.height as f32;

        let x = p.left * (1. - cx) + p.right * cx;
        let y = p.top * (1. - cy) + p.bottom * cy;
        let z = p.near;
        let w = p.right - p.left;
        let h = p.bottom - p.top;

        CameraRay {
            origin: i * Point3::origin(),
            matrix: Matrix3::from_columns(&[
                i.rotation * vec3(w / self.width as f32, 0., 0.),
                i.rotation * vec3(0., h / self.height as f32, 0.),
                i.rotation * vec3(x, y, z),
            ]),
        }
    }
}

trait Distr<X, R: Rng> {
    fn sample(&self, rng: &mut R) -> (f32, X);
}

#[derive(Clone, Debug, Copy)]
pub struct CameraRay {
    origin: Point3<f32>,
    matrix: Matrix3<f32>,
}

impl CameraRay {
    pub fn center(&self) -> CameraVertex {
        CameraVertex {
            origin: self.origin,
            dir: (self.matrix * vec3(0.5, 0.5, 1.)).normalize(),
        }
    }
}

impl<R: Rng> Distr<CameraVertex, R> for CameraRay {
    fn sample(&self, rng: &mut R) -> (f32, CameraVertex) {
        (
            1.,
            CameraVertex {
                origin: self.origin,
                dir: (self.matrix * vec3(rng.gen(), rng.gen(), 1.)).normalize(),
            },
        )
    }
}

pub fn rng_at<H: Hash>(seed: H, x: H, y: H) -> impl Rng {
    use rand::SeedableRng;

    let mut hash = fnv::FnvHasher::default();
    seed.hash(&mut hash);
    x.hash(&mut hash);
    y.hash(&mut hash);
    rand::rngs::SmallRng::seed_from_u64(hash.finish())
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, DeviceCopy)]
#[repr(u8)]
pub enum Filtering {
    Closest = 1,
    Bilinear = 2,
}

#[derive(Copy, Clone, Debug, DeviceCopy)]
#[repr(C)]
pub struct Sky<B> {
    pub radiance: B,
    pub width: u32,
    pub height: u32,
    pub units: f32, // units * mean(energy) * 4pi = watts / m^2
    pub filtering: Filtering,
}

impl<B> Sky<B> {
    #[inline(always)]
    pub fn width(&self) -> usize {
        self.width as usize
    }

    #[inline(always)]
    pub fn height(&self) -> usize {
        self.height as usize
    }

    pub fn map_buffer<A>(&self, map: &mut impl FnMut(&B) -> A) -> Sky<A> {
        Sky {
            radiance: map(&self.radiance),
            width: self.width,
            height: self.height,
            units: self.units,
            filtering: self.filtering,
        }
    }

    pub fn map_buffer_mut<A>(&mut self, map: &mut impl FnMut(&mut B) -> A) -> Sky<A> {
        Sky {
            radiance: map(&mut self.radiance),
            width: self.width,
            height: self.height,
            units: self.units,
            filtering: self.filtering,
        }
    }
}

impl<B: Deref<Target = [f32]>> Sky<B> {
    // watts / steradian / m^2
    pub fn index(&self, index: IVec2) -> Spectrum {
        let mut i = (index.x % self.width()) + index.y.min(self.height() - 1) * self.width();
        i *= 3;
        na::Vector3::from_column_slice(&self.radiance[i..][..3]) * self.units
    }

    pub fn index_filtered(&self, index: Vec2) -> Spectrum {
        match self.filtering {
            Filtering::Closest => self.index(index.map(|x| Float::round(x) as usize)),
            Filtering::Bilinear => {
                let i = index.map(|x| Float::floor(x) as usize);
                let s = index.map(Float::fract);
                let t = s.map(|x| 1. - x);

                self.index(i) * t.x * t.y
                    + self.index(i + IVec2::x()) * s.x * t.y
                    + self.index(i + IVec2::y()) * t.x * s.y
                    + self.index(i + vec2(1, 1)) * s.x * s.y
            }
        }
    }

    pub fn unit_x(&self) -> f32 {
        2. * PI / self.width as f32
    }

    pub fn unit_y(&self) -> f32 {
        PI / self.height as f32
    }

    pub fn direction_to(&self, index: Vec2) -> Vec3 {
        let azimuth = index.x * self.unit_x() - PI;
        let altitude = index.y * self.unit_y() - HALF_PI;
        let (y, x) = azimuth.sin_cos();
        let (z, r) = altitude.sin_cos();
        vec3(x * r, y * r, -z)
    }

    pub fn looking_at(&self, dir: Vec3) -> Vec2 {
        let mut azimuth = dir.y.atan2(dir.x) + PI;
        azimuth /= self.unit_x();
        let mut altitude = (-dir.z).atan2(dir.xy().magnitude()) + HALF_PI;
        altitude /= self.unit_y();
        vec2(azimuth, altitude)
    }

    pub fn look(&self, dir: Vec3) -> Spectrum {
        self.index_filtered(self.looking_at(dir))
    }

    pub fn irradiance_between(&self, start: IVec2, end: IVec2) -> Spectrum {
        let mut acc = Vec3::zeros();
        let unit_y = self.unit_y();
        for y in start.y..end.y {
            let altitude = (y as f32 + 0.5) * unit_y;
            let unit_x = altitude.sin() * self.unit_x();
            let unit_area = unit_x * unit_y;
            for x in start.x..end.x {
                acc += self.index(vec2(x, y)) * unit_area;
            }
        }
        acc
    }
}

#[derive(Copy, Clone)]
pub struct CdfSlice<'a>(&'a [f32]);

impl<'a> CdfSlice<'a> {
    fn get(self, idx: usize) -> f32 {
        self.0.get(idx).copied().unwrap_or(1.)
    }

    fn p(self, idx: usize) -> f32 {
        let above = self.get(idx);
        let below = idx.checked_sub(1).map(|i| self.get(i)).unwrap_or(0.);

        (above - below) * self.0.len() as f32
    }
}

impl<'a, R: Rng> Distr<usize, R> for CdfSlice<'a> {
    fn sample(&self, rng: &mut R) -> (f32, usize) {
        use core::cmp::Ordering::*;

        let goal: f32 = rng.sample(Open01);
        let idx = self
            .0
            .binary_search_by(|&x| if x < goal { Less } else { Greater })
            .unwrap_err();

        (self.p(idx), idx)
    }
}

#[derive(Copy, Clone, Debug, DeviceCopy)]
#[repr(C)]
pub struct SkyDistr<B> {
    sky: Sky<B>,
    block_width: u32,
    block_height: u32,
    cdf_row_sums: B,
    blocks_per_row: u32,
    cdf_rows: B,
    irradiance: f32,
}

impl<B> SkyDistr<B> {
    pub fn map_buffer<A>(&self, map: &mut impl FnMut(&B) -> A) -> SkyDistr<A> {
        SkyDistr {
            sky: self.sky.map_buffer(map),
            block_width: self.block_width,
            block_height: self.block_height,
            cdf_row_sums: map(&self.cdf_row_sums),
            blocks_per_row: self.blocks_per_row,
            cdf_rows: map(&self.cdf_rows),
            irradiance: self.irradiance,
        }
    }

    pub fn map_buffer_mut<A>(&mut self, map: &mut impl FnMut(&mut B) -> A) -> SkyDistr<A> {
        SkyDistr {
            sky: self.sky.map_buffer_mut(map),
            block_width: self.block_width,
            block_height: self.block_height,
            cdf_row_sums: map(&mut self.cdf_row_sums),
            blocks_per_row: self.blocks_per_row,
            cdf_rows: map(&mut self.cdf_rows),
            irradiance: self.irradiance,
        }
    }
}

impl<B: DerefMut<Target = [f32]>> SkyDistr<B> {
    pub fn new(
        sky: Sky<B>,
        mut allocate: impl FnMut(usize) -> B,
        mut blocks_h: usize,
        mut blocks_v: usize,
    ) -> Self {
        let block_width = (sky.width() / blocks_h).max(1);
        let block_height = (sky.height() / blocks_v).max(1);
        blocks_h = sky.width() / block_width;
        blocks_v = sky.height() / block_height;
        let num_blocks = blocks_h * blocks_v;

        let mut cdf_row_sums = allocate(blocks_v);
        let mut cdf_rows = allocate(num_blocks);
        let mut x = 0;
        let mut y = 0;
        let mut row_sum = 0.;
        loop {
            if x >= blocks_h {
                cdf_row_sums[y] = row_sum;
                row_sum = 0.;
                x = 0;
                y += 1;
            }
            if y >= blocks_v {
                break;
            }
            let sky_x = x * block_width;
            let sky_y = y * block_height;
            let radiance = sky
                .irradiance_between(
                    vec2(sky_x, sky_y),
                    vec2(
                        sky.width().min(sky_x + block_width),
                        sky.height().min(sky_y + block_height),
                    ),
                )
                .sum();
            row_sum += radiance;
            cdf_rows[x + y * blocks_h] = radiance;
            x += 1;
        }

        let total = cdf_row_sums.iter().sum::<f32>();

        let divisor = total.recip();
        let mut partial_sum = 0.;
        for x in &mut *cdf_row_sums {
            partial_sum += *x;
            *x = partial_sum * divisor;
        }

        for row in cdf_rows.chunks_mut(blocks_h) {
            let divisor = row.iter().sum::<f32>().recip();
            let mut partial_sum = 0.;
            for x in row {
                partial_sum += *x;
                *x = partial_sum * divisor;
            }
        }

        SkyDistr {
            sky,
            block_width: block_width as u32,
            block_height: block_width as u32,
            cdf_row_sums,
            blocks_per_row: blocks_h as u32,
            cdf_rows,
            irradiance: total,
        }
    }
}

impl<B: Deref<Target = [f32]>> SkyDistr<B> {
    pub fn cdf_row_sums(&self) -> CdfSlice {
        CdfSlice(&self.cdf_row_sums)
    }

    pub fn cdf_rows(&self, row: usize) -> CdfSlice {
        let w = self.blocks_per_row as usize;
        CdfSlice(&self.cdf_rows[row * w..][..w])
    }

    pub fn look(&self, dir: Vec3) -> (f32, Spectrum) {
        let i = self.sky.looking_at(dir);
        let energy = self.sky.index_filtered(i);
        let block_x = i.x as usize / self.block_width as usize;
        let block_y = i.y as usize / self.block_height as usize;
        let p0 = self.cdf_row_sums().p(block_y);
        let p1 = self.cdf_rows(block_y).p(block_x);
        (p0 * p1 / (4. * PI), energy)
    }
}

impl<B> AsRef<Sky<B>> for SkyDistr<B> {
    fn as_ref(&self) -> &Sky<B> {
        &self.sky
    }
}

impl<R: Rng, B: Deref<Target = [f32]>> Distr<SkyVertex, R> for SkyDistr<B> {
    fn sample(&self, rng: &mut R) -> (f32, SkyVertex) {
        let (p0, row) = self.cdf_row_sums().sample(rng);
        let (p1, col) = self.cdf_rows(row).sample(rng);
        let mut block_w = self.block_width as usize;
        let mut block_h = self.block_height as usize;
        let corner = vec2(col * block_w, row * block_h);
        block_w = block_w.min(self.sky.width() - corner.x);
        block_h = block_h.min(self.sky.height() - corner.y);
        let i = corner.map(|x| x as f32)
            + vec2(
                rng.gen_range(0., block_w as f32),
                rng.gen_range(0., block_h as f32),
            );

        (
            p0 * p1 / (4. * PI),
            SkyVertex {
                energy: self.sky.index_filtered(i),
                direction: self.sky.direction_to(i),
            },
        )
    }
}
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Strategy {
    LightToSurface,
    DiffuseToLight,
    GlossToLight,
}

use Strategy::*;

impl Strategy {
    pub const ALL: &'static [Strategy] = &[LightToSurface, DiffuseToLight, GlossToLight];
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, DeviceCopy)]
#[repr(C)]
pub struct StrategySamples {
    pub light_to_surface: u32,
    pub diffuse_to_light: u32,
    pub gloss_to_light: u32,
}

impl StrategySamples {
    pub fn total(self) -> u32 {
        self.light_to_surface + self.diffuse_to_light + self.gloss_to_light
    }

    pub fn iter(self) -> impl Iterator<Item=Strategy> {
        use core::iter::repeat;

        // TODO: more efficient iterator?
        repeat(LightToSurface)
            .take(self.light_to_surface as usize)
            .chain(repeat(DiffuseToLight).take(self.diffuse_to_light as usize))
            .chain(repeat(GlossToLight).take(self.gloss_to_light as usize))
    }
}

impl core::ops::Index<Strategy> for StrategySamples {
    type Output = u32;

    fn index(&self, strat: Strategy) -> &u32 {
        match strat {
            LightToSurface => &self.light_to_surface,
            DiffuseToLight => &self.diffuse_to_light,
            GlossToLight => &self.gloss_to_light,
        }
    }
}

trait Vec3Ext {
    fn cos_theta(&self) -> f32;
    fn cos2_theta(&self) -> f32;
    fn abs_cos_theta(&self) -> f32;
    fn sin2_theta(&self) -> f32;
    fn sin_theta(&self) -> f32;
    fn tan_theta(&self) -> f32;
    fn cos_sin_phi(&self) -> Vec2;
}

impl Vec3Ext for Vec3 {
    fn cos_theta(&self) -> f32 { self.z }
    fn cos2_theta(&self) -> f32 { self.z.sqr() }
    fn abs_cos_theta(&self) -> f32 { self.z.abs() }
    fn sin2_theta(&self) -> f32 { (1. - self.cos2_theta()).max(0.) }
    fn sin_theta(&self) -> f32 { self.sin2_theta().sqrt() }
    fn tan_theta(&self) -> f32 { self.sin_theta() / self.cos_theta() }

    fn cos_sin_phi(&self) -> Vec2 {
        let s = self.sin_theta();
        if s < 1e-9 {
            Vec2::x()
        } else {
            (self.xy() / s).map(|x| x.min(1.).max(-1.))
        }
    }
}

trait FloatExt: na::Scalar {
    fn sqr(self) -> Self;
    fn cos_sin(self) -> na::Vector2<Self>;
}

impl<F: Float + na::Scalar> FloatExt for F {
    fn sqr(self) -> Self { self * self }
    fn cos_sin(self) -> na::Vector2<Self> {
        let (y, x) = self.sin_cos();
        vec2(x, y)
    }
}


struct CosineHemisphere;

impl<R: Rng> Distr<Vec3, R> for CosineHemisphere {
    fn sample(&self, rng: &mut R) -> (f32, Vec3) {
        let u: f32 = rng.gen();
        let v: f32 = rng.gen();
        let r = u.sqrt();
        let theta = 2. * PI * v;
        let (x, y) = theta.sin_cos();
        let z = (1. - u).sqrt();

        (z / PI, vec3(r * x, r * y, z))
    }
}

trait MicrofacetModel {
    fn lambda(&self, dir: Vec3) -> f32;
    fn d(&self, half: Vec3) -> f32;
    fn sample(&self, out: Vec3, u: Vec2) -> Vec3;

    fn g1(&self, out: Vec3) -> f32 {
        (1. + self.lambda(out)).recip()
    }

    fn g(&self, out: Vec3, input: Vec3) -> f32 {
        (1. + self.lambda(out) + self.lambda(input)).recip()
    }

    fn pdf(&self, out: Vec3, half: Vec3) -> f32 {
        self.d(half) * self.g1(out) * out.dot(&half) / out.abs_cos_theta()
    }
}

struct Ggx {
    alpha: Vec2,
}

// TODO: this is broken or something
impl MicrofacetModel for Ggx {
    fn lambda(&self, direction: Vec3) -> f32 {
        let a2 = self.alpha.component_mul(&self.alpha);
        let v2 = direction.component_mul(&direction);
        ((1. + a2.dot(&v2.xy()) / v2.z).sqrt() - 1.) / 2.
    }

    fn d(&self, normal: Vec3) -> f32 {
        let n2 = normal.component_mul(&normal);
        let a2 = self.alpha.component_mul(&self.alpha);

        let d_inner = n2.x / a2.x + n2.y / a2.y + n2.z;
        (PI * self.alpha.x * self.alpha.y * d_inner * d_inner).recip()
    }

    fn sample(&self, out: Vec3, u: Vec2) -> Vec3 {
        // Sample the VNDF for the GGX microfacet model
        // See http://jcgt.org/published/0007/04/01/paper.pdf page 10

        // Section 3.2: transforming the view direction to the hemisphere configuration
        let a = self.alpha;
        let a3 = vec3(a.x, a.y, 1.);
        let flip = out.z < 0.;
        let v = if flip { -out } else { out };
        let vh = a3.component_mul(&v).normalize();

        // Section 4.1: orthonormal basis (with special case if cross product is zero)
        let mut t1_v = vec3(-vh.y, vh.x, 0.);
        t1_v = t1_v.try_normalize(0.0001).unwrap_or_else(Vec3::x);
        let t2_v = vh.cross(&t1_v);
        let tm = Matrix3::from_columns(&[
            t1_v,
            t2_v,
            vh,
        ]);

        // Section 4.2: parameterization of the projected area
        let r = u.x.sqrt();
        let phi = 2. * PI * u.y;
        let (mut t2, mut t1) = phi.sin_cos();
        t1 *= r;
        t2 *= r;
        let s = 0.5 * (1. + vh.z);
        t2 = (1. - s) * (1. - t1 * t1).sqrt() + s * t2;

        // Section 4.3: reprojection onto hemisphere
        let t3 = (1. - vec2(t1, t2).norm_squared()).max(0.).sqrt();
        let nh = tm * vec3(t1, t2, t3);

        // Section 3.4: transforming the normal back to the ellipsoid configuration
        let mut ne = a3.component_mul(&nh).normalize();
        ne.z = ne.z.max(0.);

        if flip { -ne } else { ne }
    }
}
struct TrowbridgeReitz {
    alpha: Vec2,
}

impl TrowbridgeReitz {
    fn sample11(w: Vec3, u: Vec2) -> Vec2 {
        // special case (normal incidence)
        if w.cos_theta() > 0.9999 {
            let r = u.x / (1.  - u.x);
            let phi = 2. * PI * u.y;
            return r * phi.cos_sin();
        }

        let tan_theta = w.tan_theta();
        let little_a = tan_theta.recip();
        let g1 = 2. / (1. + (1. + little_a.sqr().recip()).sqrt());

        // sample slope_x
        let a = 2. * u.x / g1 - 1.;
        let tmp = (a.sqr() - 1.).recip().min(1e9);
        let b = tan_theta;
        let d = b.sqr() * tmp.sqr() - (a.sqr() - b.sqr()) * tmp;
        let d = d.max(0.).sqrt();
        let slope_x = Vec2::from_element(b * tmp) + vec2(-d, d);
        let slope_x = if a < 0. || slope_x.y > tan_theta.recip() {
            slope_x.x
        } else {
            slope_x.y
        };

        // sample slope_y
        let (s, v) = if u.x > 0.5 {
            (1., 2. * (u.y - 0.5))
        } else {
            (-1., 2. * (0.5 - u.y))
        };
        let z = (v * (v * (v * 0.27385 - 0.73369) + 0.46341)) /
            (v * (v * (v * 0.093073 + 0.309420) - 1.) + 0.597999);
        let slope_y = s * z * (1.  + slope_x.sqr()).sqrt();

        vec2(slope_x, slope_y)
    }
}

impl MicrofacetModel for TrowbridgeReitz {
    fn d(&self, half: Vec3) -> f32 {
        let tan2_theta = half.tan_theta().sqr();
        if tan2_theta > 1e9 { return 0.; }
        let mut cos4_theta = half.cos2_theta();
        cos4_theta *= cos4_theta;

        let alpha_neg2 = self.alpha.map(f32::sqr).map(f32::recip);
        let e = half.cos_sin_phi().map(f32::sqr).dot(&alpha_neg2) * tan2_theta;
        (PI * self.alpha.x * self.alpha.y * cos4_theta * (e + 1.).sqr()).recip()
    }

    fn lambda(&self, w: Vec3) -> f32 {
        let abs_tan_theta = w.tan_theta().abs();
        if abs_tan_theta > 1e9 { return 0.; }
        // Compute _alpha_ for direction _w_
        let alpha2 = self.alpha.map(f32::sqr);
        let alpha = w.cos_sin_phi().dot(&alpha2);
        let a2t2t = (alpha * abs_tan_theta).sqr();
        return ((1. + a2t2t).sqrt() - 1.) / 2.;
    }

    fn sample(&self, out: Vec3, u: Vec2) -> Vec3 {
        let flip = out.z < 0.;
        let v = if flip { -out } else { out };
        let scale = vec3(self.alpha.x, self.alpha.y, 1.);
        let v = v.component_mul(&scale).normalize();
        let phi = v.cos_sin_phi();
        let slope = na::Matrix2::new(phi.x, -phi.y, phi.y, phi.x) * TrowbridgeReitz::sample11(v, u);
        let normal = vec3(-slope.x, -slope.y, 1.).component_mul(&scale).normalize();
        if flip { -normal } else { normal }
    }
}

fn shlick_fresnel(cos_theta: f32, f0: f32) -> f32 {
    f0 + (1. - cos_theta).powi(5) * (1. - f0)
}

struct Path {
    pub actual: Strategy,
    pub p_light_to_surface: f32,
    pub p_diffuse_to_light: f32,
    pub p_gloss_to_light: f32,
    pub energy: Spectrum,
}

impl core::ops::Index<Strategy> for Path {
    type Output = f32;

    fn index(&self, strat: Strategy) -> &f32 {
        match strat {
            LightToSurface => &self.p_light_to_surface,
            DiffuseToLight => &self.p_diffuse_to_light,
            GlossToLight => &self.p_gloss_to_light,
        }
    }
}


impl Path {
    /// = Weight (multiple importance sampling) / sample probability (monte carlo)
    ///
    /// Assumes strategies are all equally used
    fn multiplier(&self, samples: StrategySamples) -> f32 {
        fn power_beta(x: f32) -> f32 {
            x * x
        }

        fn power_beta_minus_one(x: f32) -> f32 {
            x
        }

        // Based on:
        // - http://www.pbr-book.org/3ed-2018/Light_Transport_III_Bidirectional_Methods/Bidirectional_Path_Tracing.html#MultipleImportanceSampling
        // - http://www.pbr-book.org/3ed-2018/Monte_Carlo_Integration/Importance_Sampling.html
        // - https://graphics.stanford.edu/courses/cs348b-03/papers/veach-chapter9.pdf

        // Notice the path is only either s=0, t=2 or s=1, t=1. That simplifies
        // things greatly. In fact, we'll use the power heuristic rather than the
        // balance heuristic and add a second camera -> light strategy.

        let mut divisor = 0.;

        // Hypothetical strategies
        for &s in Strategy::ALL {
            divisor += power_beta(samples[s] as f32 * self[s]);
        }

        // Divide actual strategy
        power_beta_minus_one(self[self.actual])
            * power_beta(samples[self.actual] as f32)
            / divisor
    }
}

type Hit = Option<SurfaceVertex>;

fn hit_sphere(center: Point3<f32>, radius: f32, ray: Ray) -> Hit {
    // From: https://raytracing.github.io/books/RayTracingInOneWeekend.html#addingasphere
    let oc = ray.origin - center;
    let a = ray.dir.magnitude_squared();
    let b = 2. * oc.dot(&ray.dir);
    let c = oc.magnitude_squared() - radius * radius;
    let disc = b * b - 4. * a * c;
    if disc < 0. {
        return None;
    }
    let t = (-b - disc.sqrt()) / (2. * a);
    if t < 0.01 {
        return None;
    }
    let p = ray.at(t);
    let n = (p - center).normalize();
    let tan = vec3(n.y, -n.x, 0.).try_normalize(1e-4).unwrap_or_else(Vec3::x);
    Some(SurfaceVertex {
        depth: t,
        point: p,
        tangent: tan,
        normal: n,
        albedo: vec3(0.7, 0.7, 0.7),
        fresnel0: 0.75,
        roughness: 0.02,
    })
}

#[derive(Copy, Clone, Debug, DeviceCopy)]
#[repr(C)]
// TODO: rename to Scene
pub struct Engine<B> {
    pub sky: SkyDistr<B>,
    pub samples: StrategySamples,
    pub include: bool,
}

impl<B> Engine<B> {
    pub fn map_buffer<A>(&self, map: &mut impl FnMut(&B) -> A) -> Engine<A> {
        Engine {
            sky: self.sky.map_buffer(map),
            samples: self.samples,
            include: self.include,
        }
    }

    pub fn map_buffer_mut<A>(&mut self, map: &mut impl FnMut(&mut B) -> A) -> Engine<A> {
        Engine {
            sky: self.sky.map_buffer_mut(map),
            samples: self.samples,
            include: self.include,
        }
    }
}

impl<B: Deref<Target = [f32]>> Engine<B> {
    fn hit_scene(&self, ray: Ray) -> Hit {
        if !self.include {
            return None;
        }

        const R: f32 = 3.0;

        let mut hit: Hit = None;
        for &center in &[
            Point3::new(R, R, 0.),
            Point3::new(-R, R, 0.),
            Point3::new(R, -R, 0.),
            Point3::new(-R, -R, 0.),
        ] {
            if let Some(this_hit) = hit_sphere(center, 2., ray) {
                if hit.map(|h| h.depth > this_hit.depth).unwrap_or(true) {
                    hit = Some(this_hit);
                }
            }
        }

        hit
    }

    fn create_path(
        &self,
        ray: Ray,
        actual: Strategy,
        v: SurfaceVertex,
        rng: &mut impl Rng,
    ) -> Path {
        let into_world = na::Matrix3::from_columns(&[
            v.normal.cross(&v.tangent),
            v.tangent,
            v.normal,
        ]);
        let from_world = into_world.transpose();
        let output = from_world * -ray.dir;
        let mfm = TrowbridgeReitz {
            alpha: vec2(v.roughness, v.roughness),
        };

        let mut look = None;

        let input = match actual {
            LightToSurface => {
                let (p, i) = self.sky.sample(rng);
                look = Some((p, i.energy));
                from_world * i.direction
            }
            DiffuseToLight => {
                let (_, l) = Distr::sample(&CosineHemisphere, rng);
                l
            }
            GlossToLight => {
                let n = mfm.sample(output, rng.gen());
                2. * output.dot(&n) * n - output
            }
        };

        let (p_light_to_surface, incoming_energy) = look.unwrap_or_else(|| {
            self.sky.look(into_world * input)
        });


        let mut energy = Vec3::zeros();
        let cos_theta = input.z.max(0.);

        // Lambertian diffuse
        energy += incoming_energy.component_mul(&v.albedo) / PI;

        // Glossy
        let mut p_gloss_to_light = 1.;
        let half = input + output;
        if let Some(half) = half.try_normalize(1e-9) {
            // Cook-Torrance
            energy += incoming_energy * mfm.d(half)
                * shlick_fresnel(output.dot(&half), v.fresnel0)
                * mfm.g(output, input)
                / (4. * output.dot(&half) * input.dot(&half));
            p_gloss_to_light = mfm.pdf(output, half);
        }

        Path {
            energy: energy * cos_theta,
            p_light_to_surface,
            p_diffuse_to_light: cos_theta / PI,
            p_gloss_to_light,
            actual,
        }
    }

    pub fn render_ray(&self, ray: CameraRay, rng: &mut impl Rng) -> Vec3 {
        let mut acc = Vec3::zeros();
        let num = self.samples.total();
        for strategy in self.samples.iter() {
            let ray = match num {
                1 => ray.center(),
                _ => ray.sample(rng).1,
            };

            if let Some(v) = self.hit_scene(ray) {
                let path = self.create_path(ray, strategy, v, rng);
                acc += path.energy * path.multiplier(self.samples);
            } else {
                acc += self.sky.as_ref().look(ray.dir);
            }
        }
        acc / num as f32
    }
}
