use futures::future::{abortable, pending, AbortHandle};
use iced::Executor;
use smol::{run, Task};
use std::future::Future;
use std::thread::spawn;

pub struct SmolExecuter {
    shutdown: AbortHandle,
}

impl Executor for SmolExecuter {
    fn new() -> Result<Self, std::io::Error> {
        let (running, shutdown) = abortable(pending::<()>());
        spawn(move || run(running));
        Ok(SmolExecuter { shutdown })
    }

    fn spawn(&self, future: impl Send + Future<Output = ()> + 'static) {
        Task::spawn(future).detach();
    }
}

impl Drop for SmolExecuter {
    fn drop(&mut self) {
        self.shutdown.abort();
    }
}
