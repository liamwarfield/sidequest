use anyhow::{Error, Context, bail};
use futures::prelude::*;
use iced::{
    button, image as image_ui, slider, Application, Column, Command, Element, Settings, Slider,
    Text,
};
use image::{
    hdr::{HDREncoder, HdrDecoder},
    ImageBuffer, Rgb,
};
use nalgebra as na;
use sidequest_core::{self as core, Filtering, ViewParams};
use smol::Task;
use std::{convert::TryInto, fmt::Display, fs::File, path::PathBuf, sync::Arc};

mod exec;
pub mod render;

type Engine = core::Engine<Vec<f32>>;
type Sky = core::Sky<Vec<f32>>;
type SkyDistr = core::SkyDistr<Vec<f32>>;
type Render = Arc<(ViewParams, Vec<f32>)>;

pub fn main() {
    SidequestGui::run(Settings {
        window: Default::default(),
        flags: structopt::StructOpt::from_args(),
        default_font: None,
        antialiasing: false,
    })
}

fn parse_samples(s: &str) -> Result<core::StrategySamples, Error> {
    let mut parts = s.split(',');
    let samples = core::StrategySamples {
        light_to_surface: parts.next().context("missing `light to surface`")?.parse()?,
        diffuse_to_light: parts.next().context("missing `diffuse to light`")?.parse()?,
        gloss_to_light: parts.next().context("missing `gloss to light`")?.parse()?,
    };
    if let Some(extra) = parts.next() {
        bail!("unknown strategy for count: {:?}", extra);
    }
    Ok(samples)
}

#[derive(Debug, structopt::StructOpt)]
#[structopt(name = "sidequest", about = "Sam's WIP renderer")]
struct Options {
    /// Output file for renders
    #[structopt(short = "o", parse(from_os_str))]
    output: Option<PathBuf>,

    /// Environment map
    #[structopt(short = "e", parse(from_os_str))]
    environment: Option<PathBuf>,

    /// Number of samples per strategy per pixel
    #[structopt(short = "n", default_value = "5,2,1", parse(try_from_str = parse_samples))]
    samples: core::StrategySamples,

    /// Include diffuse objects in scene
    #[structopt(short = "i")]
    include: bool,
}

#[derive(Clone, Debug)]
pub enum Message {
    Noop,
    SetEngine(Engine),
    SetExposure(f32),
    SetPan(f32),
    SetTilt(f32),
    RenderDone(Render),
    TonemapDone(Render, Vec<u8>),
    SaveRender,
}

#[derive(Copy, Clone, Debug, PartialEq)]
enum TaskGuard {
    Finished,
    Running,
    RunningOutdated,
}

impl TaskGuard {
    // Returns true if the task should run now.
    fn request(&mut self) -> bool {
        use TaskGuard::*;

        let (next, run) = match *self {
            Finished => (TaskGuard::Running, true),
            _ => (TaskGuard::RunningOutdated, false),
        };
        *self = next;
        run
    }

    // Returns true if the task should be restarted.
    fn finish(&mut self) -> bool {
        use std::mem::replace;

        replace(self, TaskGuard::Finished) == TaskGuard::RunningOutdated
    }
}

struct SidequestGui {
    output: Option<PathBuf>,
    engine: Arc<Engine>,
    exposure: f32,
    elevation: f32,
    azimuth: f32,
    render: Option<Render>,
    render_guard: TaskGuard,
    preview: Option<(u32, u32, Vec<u8>)>,
    preview_guard: TaskGuard,
    exposure_state: slider::State,
    pan_state: slider::State,
    tilt_state: slider::State,
    save_btn_state: button::State,
}

impl Application for SidequestGui {
    type Executor = exec::SmolExecuter;
    type Message = Message;
    type Flags = Options;

    fn new(opts: Options) -> (SidequestGui, Command<Self::Message>) {
        let mut gui = SidequestGui {
            output: opts.output,
            engine: Arc::new(Engine {
                sky: SkyDistr::new(
                    Sky {
                        #[rustfmt::skip]
                    radiance: vec![
                        1., 1., 1.,
                        1., 1., 1.,
                        1., 1., 1.,
                        1., 1., 1.,
                        0., 1., 1.,
                        1., 0., 1.,
                        1., 1., 0.,
                        1., 1., 1.,
                        1., 0., 0.,
                        0., 1., 0.,
                        0., 0., 1.,
                        0., 0., 0.,
                    ],
                        width: 4,
                        height: 3,
                        filtering: Filtering::Bilinear,
                        units: 1.,
                    },
                    |len| vec![0.; len],
                    4,
                    3,
                ),
                samples: opts.samples,
                include: opts.include,
            }),
            exposure: 0.,
            elevation: 30.,
            azimuth: 0.,
            render: None,
            render_guard: TaskGuard::Finished,
            preview: None,
            preview_guard: TaskGuard::Finished,
            exposure_state: slider::State::new(),
            pan_state: slider::State::new(),
            tilt_state: slider::State::new(),
            save_btn_state: button::State::new(),
        };

        let mut init = vec![gui.render()];
        let samples = opts.samples;
        let include = opts.include;
        if let Some(env_path) = opts.environment {
            init.push(
                blocking_err(format!("loading {}", env_path.display()), async move {
                    use std::io::BufReader;

                    let decode = HdrDecoder::new(BufReader::new(File::open(env_path)?))?;
                    let meta = decode.metadata();
                    let mut energy = Vec::new();
                    energy.reserve(meta.width as usize * meta.height as usize * 3);
                    for p in decode {
                        energy.extend_from_slice(&p?.to_hdr().0);
                    }

                    let sky = Sky {
                        radiance: energy,
                        filtering: if meta.width > 1024 {
                            Filtering::Closest
                        } else {
                            Filtering::Bilinear
                        },
                        width: meta.width,
                        height: meta.height,
                        units: meta.exposure.unwrap_or(1.),
                    };

                    Ok(Message::SetEngine(Engine {
                        sky: SkyDistr::new(sky, |len| vec![0.; len], 256, 128),
                        samples,
                        include,
                    }))
                })
                .into(),
            );
        }

        (gui, Command::batch(init))
    }

    fn title(&self) -> String {
        String::from("sidequest")
    }

    fn update(&mut self, msg: Message) -> Command<Message> {
        use Message::*;

        match msg {
            Noop => Command::none(),
            SetEngine(e) => {
                self.engine = Arc::new(e);
                self.render()
            }
            SetExposure(x) => {
                self.exposure = x;
                self.tonemap()
            }
            SetPan(x) => {
                self.azimuth = x;
                self.render()
            }
            SetTilt(x) => {
                self.elevation = x;
                self.render()
            }
            RenderDone(v) => {
                self.render = Some(v);
                if self.render_guard.finish() {
                    Command::batch(vec![self.render(), self.tonemap()])
                } else {
                    self.tonemap()
                }
            }
            TonemapDone(v, m) => {
                self.preview = Some((v.0.width, v.0.height, m));
                if self.preview_guard.finish() {
                    self.tonemap()
                } else {
                    Command::none()
                }
            }
            SaveRender => match (&self.output, &self.render, &self.preview) {
                (Some(o), Some(r), _) if o.extension() == Some("hdr".as_ref()) => {
                    let o = o.clone();
                    let r = r.clone();
                    blocking_err("saving HDR image", async move {
                        let enc = HDREncoder::new(File::create(o)?);
                        let rgb: Vec<_> =
                            r.1.chunks(3).map(|v| Rgb(v.try_into().unwrap())).collect();
                        enc.encode(&rgb, r.0.width as usize, r.0.height as usize)?;
                        Ok(Message::Noop)
                    })
                    .into()
                }
                (Some(o), _, Some((w, h, b))) => {
                    let o = o.clone();
                    let w = *w;
                    let h = *h;
                    let b = b.clone();
                    blocking_err("saving LDR image", async move {
                        image::DynamicImage::ImageBgra8(
                            ImageBuffer::from_raw(w, h, b).expect("invalid buffer"),
                        )
                        .into_rgb()
                        .save(o)?;
                        Ok(Message::Noop)
                    })
                    .into()
                }
                _ => Command::none(),
            },
        }
    }

    fn view(&mut self) -> Element<Message> {
        let mut col = Column::new();
        if let Some((w, h, b)) = &self.preview {
            col = col.push(image_ui::Image::new(image_ui::Handle::from_pixels(
                *w,
                *h,
                b.clone(),
            )));
        }

        col = col
            .push(Slider::new(
                &mut self.tilt_state,
                -90.0..=90.0,
                self.elevation,
                |x| Message::SetTilt(x),
            ))
            .push(Slider::new(
                &mut self.pan_state,
                0.0..=360.0,
                self.azimuth,
                |x| Message::SetPan(x),
            ))
            .push(Slider::new(
                &mut self.exposure_state,
                -6.0..=6.0,
                self.exposure,
                |x| Message::SetExposure(x),
            ));

        if self.output.is_some() {
            col = col.push(
                button::Button::new(&mut self.save_btn_state, Text::new("Save"))
                    .on_press(Message::SaveRender),
            );
        }

        col.into()
    }
}

impl SidequestGui {
    fn render(&mut self) -> Command<Message> {
        if !self.render_guard.request() {
            return Command::none();
        }

        let (x, y) = self.azimuth.to_radians().sin_cos();
        let (z, r) = self.elevation.to_radians().sin_cos();
        let engine = self.engine.clone();
        let view = ViewParams {
            perspective: core::Perspective {
                left: -2.,
                right: 2.,
                top: 1.,
                bottom: -1.,
                near: 2.,
                far: 100.,
            },
            isometry: na::Isometry3::face_towards(
                &(10. * na::Point3::new(x * r, y * r, z)),
                &na::Point3::new(0., 0., 0.),
                &na::Vector3::new(0., 0., 1.),
            )
            .into(),
            width: 1024,
            height: 512,
        };
        Task::blocking(async move {
            let buf = render::render(&*engine, &view);
            Message::RenderDone(Arc::new((view, buf)))
        })
        .into()
    }

    fn tonemap(&mut self) -> Command<Message> {
        if !self.preview_guard.request() {
            return Command::none();
        }

        let view = match &self.render {
            None => return Command::none(),
            Some(v) => v.clone(),
        };
        let exposure = self.exposure.exp2();
        Task::blocking(async move {
            let mut buffer = vec![0; view.0.width as usize * view.0.height as usize * 4];
            core::aces::to_ldr(&view.1, &mut buffer, exposure);
            Message::TonemapDone(view, buffer)
        })
        .into()
    }
}

async fn blocking_err(
    ctx: impl Display,
    f: impl Future<Output = Result<Message, Error>> + Send + 'static,
) -> Message {
    match Task::blocking(f).await {
        Ok(m) => m,
        Err(e) => {
            println!("Error while {}: {:?}", ctx, e);
            Message::Noop
        }
    }
}
