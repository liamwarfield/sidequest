use sidequest_core::{self as core, Engine, ViewParams};
use anyhow::Error;

pub fn cpu_render(
    e: &Engine<Vec<f32>>,
    v: &ViewParams) -> Vec<f32>
{
    use rayon::prelude::*;

    let seed = rand::random();
    let w = v.width as usize;
    let h = v.height as usize;
    let mut acc = vec![0.; w * h * 3];
    acc.par_chunks_exact_mut(3).enumerate().for_each(|(i, o)| {
        let x = i % w;
        let y = i / w;
        let mut rng = core::rng_at(seed, x, y);
        let val: [f32; 3] = e.render_ray(v.camera_ray(x, y), &mut rng).into();
        o.copy_from_slice(&val);
    });
    acc
}

#[cfg(feature="cuda")]
pub fn cuda_render(
    e: &Engine<Vec<f32>>,
    v: &ViewParams,
) -> Result<Vec<f32>, Error> {
    use rustacuda::launch;
    use rustacuda::prelude::*;
    use rustacuda::memory::DeviceBox;
    use sidequest_core::DeviceBufferRef;
    use std::ffi::CString;

    // Create a context associated to this device
    // TODO: keep alive
    rustacuda::init(CudaFlags::empty())?;
    let device = Device::get_device(0)?;
    let _context = Context::create_and_push(
        ContextFlags::MAP_HOST | ContextFlags::SCHED_AUTO, device)?;

    // Load PTX module
    let module_data = CString::new(include_str!(env!("KERNEL_PTX_PATH")))?;
    let kernel = Module::load_from_string(&module_data)?;

    // Create a stream to submit work to
    let stream = Stream::new(StreamFlags::NON_BLOCKING, None)?;

    // Convert engine
    let mut gpu_engine = e.map_buffer(&mut |s| {
        assert!(!s.is_empty());
        DeviceBuffer::from_slice(s).expect("could not transfer engine buffers")
    }); // TODO: try

    // Create output
    let mut out = vec![0.; v.width as usize * v.height as usize * 3];
    let mut gpu_out = DeviceBuffer::from_slice(&out)?;

    // Crate parameters
    let mut params = DeviceBox::new(&core::KernelParams {
        seed: rand::random(),
        view: v.clone(),
        engine: gpu_engine.map_buffer_mut(&mut |s| {
            DeviceBufferRef::from_raw(s.as_device_ptr(), s.len() as u32)
        }),
        buffer: gpu_out.as_device_ptr(),
        len: gpu_out.len() as u32,
    })?;

    // Do rendering
    let threads = (8, 8);
    let ceil_div = |x, y| (x + y - 1) / y;
    let blocks = (ceil_div(v.width, threads.0), ceil_div(v.height, threads.1));

    unsafe {
        /* launch!(kernel.render<<<blocks, threads, 0, stream>>>(
            seed,
            gpu_e_ref,
            v.clone(),
            DeviceBufferRef::from_raw(gpu_out.as_device_ptr(), gpu_out.len() as u32)
        ))?; */

        launch!(kernel.render<<<blocks, threads, 0, stream>>>(params.as_device_ptr()))?;
    }

    // The kernel launch is asynchronous, so we wait for the kernel to finish executing
    stream.synchronize()?;

    // Copy the result back to the host
    gpu_out.copy_to(&mut out)?;

    Ok(out)
}

#[cfg(not(feature="cuda"))]
pub fn cuda_render(
    _: &Engine<Vec<f32>>,
    _: &ViewParams,
) -> Result<Vec<f32>, Error> {
    Err(anyhow::anyhow!("not compiled with the \"cuda\" feature"))
}

pub fn render(
    e: &Engine<Vec<f32>>,
    v: &ViewParams) -> Vec<f32>
{
    if e.samples.total() >= 8 {
        match cuda_render(e, v) {
            Ok(output) => return output,
            Err(e) => eprintln!("Could not use GPU rendering: {:?}", e),
        }
    }

    cpu_render(e, v)
}
